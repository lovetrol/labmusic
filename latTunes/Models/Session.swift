//
//  Session.swift
//  latTunes
//
//  Created by Marths! on 11/9/18.
//  Copyright © 2018 Marths!. All rights reserved.
//

import Foundation
class Session: NSObject{
    var token: String?
    static let sharedInstance = Session() // se crea session una sola vez y no es inmutable
    //single y solo se puede instanciar en esta clase
    override private init(){
        super.init()
    }
    
    func saveSession(){
        token = "123456789"
    }
    
}
