//
//  latTunesTests.swift
//  latTunesTests
//
//  Created by Marths! on 11/9/18.
//  Copyright © 2018 Marths!. All rights reserved.
//

import XCTest
@testable import latTunes

class latTunesTests: XCTestCase {

    override func setUp() {
        let session = Session.sharedInstance
        session.token = nil
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    func testCorrectLogin() {
        XCTAssertTrue(User.login(userName: "IOSLab", password: "verysecurepass"))
    }
    func testWrongLogin() {
        XCTAssertFalse(User.login(userName: "lala", password: "land"))
    }
    
    func testSession() {
        let session = Session.sharedInstance
        let _ = User.login(userName: "IOSLab", password: "223")
        XCTAssertNotNil(session.token)
    }
    func testSessionNot() {
        let session = Session.sharedInstance
        let _ = User.login(userName: "lala", password: "land")
        XCTAssertNil(session.token)
    }
    
    /*func testExpectedToken() {
        let _ = User.login(userName: "Johnyy", password: "333")
        let session = Session.sharedInstance
        XCTAssertEqual(session.token!, "123456789","")
        XCTAssertNotEqual(session.token!, "lala", "land")
    }*/
    
    func testThrowError() {
        XCTAssertThrowsError(try User.fetchSongs())
    }
    
    func testMusicSong(){
        var resultSong: [Song] = []
        let promise = expectation(description: "songs Downloaded")
        Music.fetchSongs{(songs) in
            resultSong = songs
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotEqual(resultSong.count, 0)
    }

}
